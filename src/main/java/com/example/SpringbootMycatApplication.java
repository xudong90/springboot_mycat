package com.example;

import com.example.common.LogUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@MapperScan("com.example.dao")
public class SpringbootMycatApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMycatApplication.class, args);
        LogUtil.info("======================== > sprintboot has started < ======================");
    }

}
