package com.example.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2019/3/15.
 * 最后实现序列化，不然很多地方可能有问题
 */
public class Student implements Serializable{
    private int id;
    private String name;
    private int age;
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Student(int id, String name, int age, String address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public Student() {
    }
    public String toString(){
        return "[id:"+this.id+",age:"+this.age+",name:"+this.name+",address:"+this.address+"]";
    }

}
