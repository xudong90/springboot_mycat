package com.example.service;


import com.example.dao.StudentMapper;
import com.example.model.Student;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/3/15.
 */
@Service
public class StudentService implements StudentMapper {
    @Autowired
    private StudentMapper studentMapper;
    @Override
    public int getCount() {
        return studentMapper.getCount();
    }

    @Override
    public List<Student> getStuList() {
        return studentMapper.getStuList();
    }

    @Override
    public int insert(Student student) {
        return studentMapper.insert(student);
    }

    @Override
    public List<Student> getStuByAge(int age) {
        return studentMapper.getStuByAge(age);
    }

    @Override
    public List<Student> getStuByIdScope(Map<String,Object> map) {
        return studentMapper.getStuByIdScope(map);
    }

    public void test(){
        Map<String,Object> map = new HashMap<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(map);
            }
        }).start();
    }
}
