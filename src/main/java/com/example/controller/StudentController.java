package com.example.controller;


import com.alibaba.fastjson.JSONObject;
import com.example.common.BeanMapUtil;
import com.example.common.JsonUtil;
import com.example.common.ResponseInfo;
import com.example.common.ResponseUtil;
import com.example.dto.StuAddDto;
import com.example.dto.StuByIdScopeDto;
import com.example.model.Student;
import com.example.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/3/15.
 */

@Api(value="StudentController",description = "学生信息相关接口，读写分离了")
@Controller
@RequestMapping("/stu")
public class StudentController {
    private static final Logger log = LoggerFactory.getLogger(StudentController.class);
    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/getCount",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation("查询所有学生数量")
    public String getStuCount(){
        System.out.print("==========");
        int count = studentService.getCount();
        String str = "总计数量："+count;
        return str;
    }
    @RequestMapping(value = "/getAllStudent",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation("得到所有学生信息列表")
    public List<Student> getAllStudent(){
        log.info("======== getAllStudent start ==============");
        List<Student> studentList = studentService.getStuList();
        log.info("=============== studentList message ==============");
        log.info("studentList: "+studentList);
        log.info("======== getAllStudent end ==============");
        return studentList;
    }
    @RequestMapping(value = "/getStuByAge/{age}",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value="查询年龄小于age的学生信息",notes = "查询年龄大于age的学生信息 ")
    //@ApiImplicitParam(name = "age", value = "年龄", required = true, dataType = "Integer")
    public List<Student> getStuByAge(@PathVariable(value="age") Integer age){
        log.info("======== getStuByAge start ==============");
        log.info("=======================age:>"+age);
        List<Student> studentList = studentService.getStuByAge(age);
        return studentList;
    }
    @RequestMapping(value = "/getStuByIdScope",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="根据Id的范围查询学生信息",notes = "json 格式 ：{}")
    public ResponseInfo<Object> getStuByIdScope(@RequestBody StuByIdScopeDto stuByIdScopeDto){
        log.info("===getStuByIdScope request data is:"+ JsonUtil.toJsonString(stuByIdScopeDto));
        ResponseInfo<Object> responseInfo = new ResponseInfo<>();
        Map<String,Object> requestMap = (Map<String,Object>) BeanMapUtil.objectToMap(stuByIdScopeDto);
        JSONObject result = new JSONObject();
        Map<String,Object> params = new HashMap<>();
        int minId = (Integer) requestMap.get("id1");
        int maxId = (Integer) requestMap.get("id2");
        params.put("minId",minId);
        params.put("maxId",maxId);
        log.info("===========minId:"+minId+",maxId: "+maxId+"==============");
        List<Student> studentList = studentService.getStuByIdScope(params);
        //将list 集合转换为jsonObject需要写工具类
        responseInfo.setData(studentList);
        responseInfo.setRtnCode(ResponseUtil.SUCCESS_CODE);
        responseInfo.setRtnMsg(ResponseUtil.SUCCESS_MSG);
        log.info(responseInfo.toString());
        return responseInfo;
    }
    @ApiOperation(value = "新增学生信息",notes = "")
    @RequestMapping(value = "/insertStu",method = RequestMethod.POST)
    @ResponseBody
    public int insertStu(@RequestBody StuAddDto stuAddDto){
        log.info("===insertStu request data is:"+ JsonUtil.toJsonString(stuAddDto));
        Map<String,Object> requestMap = (Map<String,Object>) BeanMapUtil.objectToMap(stuAddDto);
        Student student = new Student();
        int id = (int)requestMap.get("id");
        String name = (String)requestMap.get("name");
        int age = (int)requestMap.get("age");
        String address = (String)requestMap.get("address");
        student.setId(id);
        student.setName(name);
        student.setAge(age);
        student.setAddress(address);
        int count = studentService.insert(student);
        return count;
    }
    @ApiOperation(value = "测试nginx",notes = "")
    @RequestMapping(value = "/testNginx",method = RequestMethod.GET)
    @ResponseBody
    public String testNginx(){
        return "第一个服务器，用的是 8080端口服务。";
    }
}
