package com.example.common;

import com.example.controller.StudentController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogUtil {
    private static final Logger log = LoggerFactory.getLogger(StudentController.class);
    public static  void info(Object obj){
        log.info(""+obj);
    }

}
