package com.example.common;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * Created by Administrator on 2019/3/19.
 */
public class JsonUtil {
    public static String toJsonString(Object obj){
        return JSONObject.toJSONString(obj);
    }
    public static String toJsonStringWithDataFormat(Object obj){
        return JSONObject.toJSONStringWithDateFormat(obj,"yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
    }

}
