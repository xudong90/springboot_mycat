package com.example.common;

/**
 * Created by Administrator on 2019/3/19.
 */
public class ResponseInfo<T> {
    private String rtnCode;//状态码
    private String rtnMsg;//状态信息
    private T data;//数据对象

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    @Override
    public String toString(){
        return "ResponseInfo [rtnCode="+rtnCode+", rtnMsg="+rtnMsg+",data="+data+"]";
    }
}
