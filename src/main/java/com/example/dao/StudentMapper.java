package com.example.dao;



import com.example.model.Student;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/3/15.
 */

public interface StudentMapper {

    int  getCount();

    List<Student> getStuList();

    int insert(Student student);

    List<Student> getStuByAge(int age);

    List<Student> getStuByIdScope(Map<String, Object> map);

}
